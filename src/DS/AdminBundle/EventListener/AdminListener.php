<?php

namespace DS\AdminBundle\EventListener;

use DS\AdminBundle\Controller\AdminAuthenticationInterface;
use eZ\Publish\Core\MVC\Symfony\Routing\ChainRouter;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\SecurityContext;

class AdminListener
{

    /**
     * @var SecurityContext
     */
    private $securityContext;

    /**
     * @var ChainRouter
     */
    private $router;

    /**
     * @var string
     */
    private $route = 'ds_admin_login';

    /**
     * @param SecurityContext $securityContext
     * @param ChainRouter $router
     */
    public function __construct($securityContext, $router)
    {
        $this->securityContext = $securityContext;
        $this->router = $router;
    }

    public function onKernelController(FilterControllerEvent $event)
    {

        $controllers = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controllers)) {
            return;
        }
        $controller = $controllers[0];
        if ($controller instanceof AdminAuthenticationInterface) {

            $token = $this->securityContext->getToken();
            $repo = $controller->getRepository();
            $loggedUser = $repo->getUserService()->loadUserByLogin($token->getUsername());
            $repo->setCurrentUser($loggedUser);
        }
    }
}

