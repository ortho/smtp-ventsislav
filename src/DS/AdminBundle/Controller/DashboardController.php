<?php

namespace DS\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use eZ\Bundle\EzPublishCoreBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @package DS\AdminBundle\Controller
 * @Route("/admin")
 */
class DashboardController extends BaseController implements AdminAuthenticationInterface
{

    /**
     * @Route("/", name="ds_admin_dashboard")
     * @Template()
     */
    public function indexAction()
    {
        $admin = $this->getRepository()->getCurrentUser();
        return [
            'admin' => $admin
        ];
    }
}
