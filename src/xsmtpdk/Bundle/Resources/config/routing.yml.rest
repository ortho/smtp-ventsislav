#xsmtpdk_homepage:
#    pattern:  /hello/{name}
#    defaults: { _controller: xsmtpdkBundle:Default:index }

xsmtpdkBundle_hello_world:
    pattern: /xsmtpdk/hello/{name}
    defaults:
        _controller: xsmtpdkBundle.controller.default:sayHello
    methods: [GET]

xsmtpdkBundle_userexists:
    pattern: /xsmtpdk/userexists/{name}
    defaults:
        _controller: xsmtpdkBundle.controller.default:userExists
    methods: [GET]

xsmtpdkBundle_commandme:
    pattern: /xsmtpdk/commandme/{name}
    defaults:
        _controller: xsmtpdkBundle:Default:commandMe
    methods: [GET]
