<?php

namespace xsmtpdk\Bundle\Classes;

//Search includes;
use DateTime;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use eZ\Publish\Core\FieldType\Null;

//For setField values

//
class xsOrders
{
    public $contentTypeService;
    public $contentService;
    public $logger;
    public $repository;
    public $searchService;
    public $locationService;
    public $urlAliasService;
    public $userService;

    public $orders_node;
    public $ORDERS_URL = "/_XSMTPDK_/Orders";


    public function __construct(&$repository, &$logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;

        //Instantiate services;
        $this->contentTypeService = $repository->getContentTypeService();
        $this->contentService = $repository->getContentService();
        $this->locationService = $repository->getLocationService();
        $this->searchService = $repository->getSearchService();
        $this->urlAliasService = $repository->getURLAliasService();
        $this->userService = $repository->getUserService();

        //Get type subscription folder node ID
        $this->orders_node = $this->urlAliasService->lookup($this->ORDERS_URL)->destination;


    }

    /* Create a new order
    *  $xs_order_data: an array with the order details
     * Returns: integer (the orderID of the newly created order)
    */
    public function createOrder($xs_order_data = array())
    {

        /* GOD MODE ON */

        $adminUser = $this->userService->loadUserByCredentials("admin", "publish");
        $this->repository->setCurrentUser($adminUser);


        ///Creating the order
        $orderType = $this->contentTypeService->loadContentTypeByIdentifier('xs_order');
        $contentOrderStruct = $this->contentService->newContentCreateStruct($orderType, 'eng-GB');

        if (!isset($xs_order_data['orderid'])) $xs_order_data['orderid'] = $this->newOrderID();

        if (!isset($xs_order_data['date'])) $xs_order_data['date'] = new DateTime(date("Y-m-d H:i:s"));

        //Set all the fields in $contentOrder
        foreach ($xs_order_data as $key => $value) {
            if (in_array($key, array('user_admin_email', 'payment_method', 'transactionid', 'currency'))) $value = strtolower($value);
            elseif (in_array($key, array('orderid', 'total'))) $value = (int)$value;
            elseif (in_array($key, array('embedded_data'))) $value = json_encode($value);
            $contentOrderStruct->setField($key, $value);
        }

        //Create the order object
        $contentOrder = $this->contentService->createContent($contentOrderStruct, array($this->locationService->newLocationCreateStruct($this->orders_node)));
        $pubContentOrder = $this->contentService->publishVersion($contentOrder->getVersionInfo());

        return ($xs_order_data['orderid']);
    }

    /* Get all data for an order
    *  $orderid: the field ID of the order (it is supposed to be unique)
     * Returns: array (the complete order data)
    */
    public function getOrder($orderid)
    {
        /* GOD MODE ON */

        $adminUser = $this->userService->loadUserByCredentials("admin", "publish");
        $this->repository->setCurrentUser($adminUser);

        //Making a query search for all the orders in the orders node
        $crit = new Criterion\LogicalAnd(
            array(
                new Criterion\ContentTypeIdentifier("xs_order"),
                new Criterion\Field("orderid", Criterion\Operator::EQ, $orderid),
                new Criterion\ParentLocationId($this->orders_node)
            )
        );

        //try catch
        // one catch is enough
        try {
            $oneResult = $this->searchService->findSingle($crit);
        } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
            return false;
        } catch (\eZ\Publish\API\Repository\Exceptions\InvalidArgumentException $e) {
            return false;
        }

        //Retrieve order data
        $anOrder = array(
            //Info: http://pubsvn.ez.no/doxygen/5.1/NS/html/classeZ_1_1Publish_1_1API_1_1Repository_1_1Values_1_1Content_1_1ContentInfo.html
            'orderid' => $oneResult->getFieldValue('orderid')->__toString(),
            'date' => $oneResult->getFieldValue('date')->__toString(),
            'user_admin_email' => $oneResult->getFieldValue('user_admin_email')->__toString(),
            'payment_method' => $oneResult->getFieldValue('payment_method')->__toString(),
            'transactionid' => $oneResult->getFieldValue('transactionid')->__toString(),
            'total' => $oneResult->getFieldValue('total')->__toString(),
            'currency' => $oneResult->getFieldValue('currency')->__toString(),
            'status' => $oneResult->getFieldValue('status')->__toString(),
            'embedded_data' => json_decode($oneResult->getFieldValue('embedded_data')->__toString(), true) //true here means convert to array
        );
        return $anOrder;
    }

    /* Cleanup incomplete orders
    *  $user_admin_email: the email of the user to be looked for pending orders
     * Returns: integer (the number of deleted orders)
    */
    public function cleanupPendingOrders($user_admin_email)
    {
        /* GOD MODE ON */

        $adminUser = $this->userService->loadUserByCredentials("admin", "publish");
        $this->repository->setCurrentUser($adminUser);

        $deleted_counter = 0;
        //Making a query search for all the orders in the orders node
        $query = new Query();
        $query->criterion = new Criterion\LogicalAnd(
            array(
                new Criterion\ContentTypeIdentifier("xs_order"),
                new Criterion\Field("user_admin_email", Criterion\Operator::EQ, $user_admin_email),
                new Criterion\Field("status", Criterion\Operator::EQ, 'pending'),
                new Criterion\ParentLocationId($this->orders_node)
            )
        );

        //try catch
        try {
            $searchResult = $this->searchService->findContent($query);
        } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
            return $deleted_counter;
        }
        /*
        } catch (\eZ\Publish\API\Repository\Exceptions\InvalidArgumentException $e) {
            return $defaultOrderID;
        }
        */
        //Remove all matching orders one by one

        foreach ($searchResult->searchHits as $elem) {
            $this->contentService->deleteContent($elem->valueObject->getVersionInfo()->getContentInfo());
            $deleted_counter++;
        }
        return $deleted_counter;
    }

    /*
     * Generate the new Order ID that should be associated to a new order (next available orderid number)
     * Returns: integer
     */
    public function newOrderID()
    {
        $defaultOrderID = 1;
        //Making a query search for all the orders in the orders node
        $query = new Query();
        $query->criterion = new Criterion\LogicalAnd(
            array(
                new Criterion\ContentTypeIdentifier("xs_order"),
                new Criterion\ParentLocationId($this->orders_node)
            )
        );
        //Priority Sort Order (Descending)
        $query->sortClauses = array(new SortClause\Field('xs_order', 'orderid', Query::SORT_DESC, 'eng-GB'));
        //And we only need one result, the one with MAX value of field orderid
        $query->limit = 1;

        //try catch
        // one catch is what we want, otherwise return the defaultOrderID
        try {
            $searchResult = $this->searchService->findContent($query);
        } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
            return $defaultOrderID;
        }
        /*
        } catch (\eZ\Publish\API\Repository\Exceptions\InvalidArgumentException $e) {
            return $defaultOrderID;
        }
        */
        //A redundant foreach, as only one content object should be here
        foreach ($searchResult->searchHits as $elem) {
            $lastOrderID = (int)$elem->valueObject->getFieldValue('orderid')->__toString();
            //\Doctrine\Common\Util\Debug::dump($elem->valueObject->getFieldValue('embedded_data'));
        }
        //If the $lastOrderID is below the defaultOrderID then return that one
        if ($defaultOrderID > $lastOrderID) return $defaultOrderID;
        else return ($lastOrderID + 1);

    }
}
