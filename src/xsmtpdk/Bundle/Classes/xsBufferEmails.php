<?php

namespace xsmtpdk\Bundle\Classes;

//Search includes;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;

//
class xsBufferEmails
{
    public $repository;
    public $searchService;
    public $locationService;
    public $urlAliasService;
    public $contentTypeService;

    public $buffer_emails_node;
    public $BUFFER_EMAILS_URL = "/_XSMTPDK_/BufferEmails/";

    public function __construct(&$repository)
    {
        $this->repository = $repository;

        //Instantiate services;
        $this->searchService = $repository->getSearchService();
        $this->locationService = $repository->getLocationService();
        $this->urlAliasService = $repository->getURLAliasService();
        $this->contentTypeService = $repository->getContentTypeService();

        //Get type BUFFER folder node ID
        $this->buffer_emails_node = $this->urlAliasService->lookup($this->BUFFER_EMAILS_URL)->destination;

    }

    ///////////////////////////////////////
    //Retrieves an array with available bufferEmails
    public function listBufferEmails()
    {
        //Making a query of the buffer emails in the node
        $query = new Query;
        $query->criterion = new Criterion\LogicalAnd(
            array(
                new Criterion\ContentTypeIdentifier("xs_buffer_emails"),
                new Criterion\ParentLocationId($this->buffer_emails_node),
            )
        );
        //Priority Sort Order (Ascending)
        $query->sortClauses = array(new SortClause\Field('xs_buffer_emails', 'no_relays', Query::SORT_ASC, 'eng-GB'));
        $searchResult = $this->searchService->findContent($query);
        $bufferEmails = array();

        foreach ($searchResult->searchHits as $elem) {
            $no_relays = $elem->valueObject->getFieldValue('no_relays')->__toString();
            $bufferEmails[$no_relays] = array(
                "no_relays" => $no_relays,
                "eur" => $elem->valueObject->getFieldValue('eur')->__toString(),
                "dkk" => $elem->valueObject->getFieldValue('dkk')->__toString()
            );
        }
        return $bufferEmails;
    }

}
