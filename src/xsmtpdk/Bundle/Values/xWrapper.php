<?php
namespace xsmtpdk\Bundle\Values;

class xWrapper
{
    public $reqname;
    public $responsevalue;

    public function __construct($reqname, $responsevalue)
    {
        $this->reqname = $reqname;
        $this->responsevalue = $responsevalue;
    }
}
