<?php

namespace xsmtpdk\Bundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use eZ\Bundle\EzPublishCoreBundle\Controller;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use xsmtpdk\Bundle\Classes\xsBufferEmails;
use xsmtpdk\Bundle\Classes\xsCaptcha;
use xsmtpdk\Bundle\Classes\xsOrders;
use xsmtpdk\Bundle\Classes\xsSubscription;
use xsmtpdk\Bundle\Classes\xsUsers;
use xsmtpdk\Bundle\Classes\xsWrapper;

//Classes

//Search includes;


//////////////////////////////////////////
class DefaultController extends Controller
{
    public $epayMD5Key = "n3w5h1t";
    /*
    public function userexistsAction( $name)
    {

	$repository=$this->getRepository();
	$userService = $repository->getUserService();
	try
	{
	    $user=$userService->loadUserByLogin($name);
	    //XPL_PATCH debugul necesar
	    \Doctrine\Common\Util\Debug::dump($user);

	}
	catch( \eZ\Publish\API\Repository\Exceptions\NotFoundException $e )
	{
	    // if the id is not found
	    return new Response(xsWrapper::assemble('userexists', $name, 'user not found')->toJson());
	}
	return new Response(xsWrapper::assemble('userexists', $name, get_object_vars($user))->toJson());
    }
    */

    /**
     * Gets the controller's container (necessary for the Controller to run also as a service)
     *
     * @return \Symfony\Component\DependencyInjection\Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /*Service function urltranslator
    // Untested twig extension: <a href="{{ path( 'ez_urlalias', {'locationId': 123} ) }}">Go</a>
    */
    //////////////////////////////////////////////
    public function urltranslator($url, $language)
    {
        $url = trim(strtolower($url));
        $language = trim(strtolower($language));
        $logger = $this->get('xsmtpdk_logger'); //Custom logger
        $repository = $this->getRepository(); //get the repo
        $urlAliasService = $repository->getURLAliasService(); //get URLAlias service
        $locationService = $repository->getLocationService(); //get Location service
        $found_node = $urlAliasService->lookup($url)->destination; //obtain the node of the path
        $location = $locationService->loadLocation($found_node, $language); //create a location object from the node
        $translated_path = $urlAliasService->reverseLookup($location)->path; //make a reverse lookup, and get the translated path
        //XPL_DEBUG
        //$logger->debug(__METHOD__."\n"."Lookup node below");
        //$logger->debug(__METHOD__."\n".print_r($translated_path,true));
        return new Response($translated_path);
    }

    /*Service function generatecaptcha64
    // Returns a base64 encoded image png image binary with the generated captcha code
    */
    //////////////////////////////////////////////
    public function generatecaptcha64()
    {
        $logger = $this->get('xsmtpdk_logger'); //Custom logger
        $session = $this->get('session'); //get the session object

        $xsCaptcha = new xsCaptcha($session, $logger);
        $xsCaptchaImage = $xsCaptcha->generateImage();

        //XPL_DEBUG
        $logger->debug(__METHOD__ . " New captcha code: " . $xsCaptchaImage['code'] . " for sessid: " . $session->getId());
        //

        return new Response($xsCaptchaImage['imdata_png_base64']);
    }

    public function sendcaptchaAction()
    {

        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Ldn3AMTAAAAACVI22Q1NO6sHvIyVwmMFUqB8-Xx&response=" . $_REQUEST['response']);
        return new Response($response);
    }

    //////////////////////////////////////////////
    public function verifycaptchacodeAction($captcha_code)
    {
        $logger = $this->get('xsmtpdk_logger'); //Custom logger
        $session = $this->get('session'); //get the session object

        $xsCaptcha = new xsCaptcha($session, $logger);

        if ($xsCaptcha->verifyCode($captcha_code)) {
            return new Response(xsWrapper::assemble('validcaptchacode', $captcha_code, xsWrapper::YES)->toJson());
        } else {
            return new Response(xsWrapper::assemble('validcaptchacode', $captcha_code, xsWrapper::NO)->toJson());
        }
    }

    //////////////////////////////////////////////
    public function userexistsAction($name)
    {

        $name = trim(strtolower($name));  //Format the string first
        $repository = $this->getRepository(); //get the repo
        $logger = $this->get('xsmtpdk_logger'); //Custom logger

        $xsUsers = new xsUsers($repository, $logger, $this);
        //XPL_DEBUG
        $logger->debug(__METHOD__ . "\n" . "Searching for user login: $name");
        //\\//
        if ($xsUsers->userExists($name)) {
            return new Response(xsWrapper::assemble('userexists', $name, xsWrapper::YES)->toJson());
        } else {
            return new Response(xsWrapper::assemble('userexists', $name, xsWrapper::NO)->toJson());
        }
    }

    //////////////////////////////////////////////
    public function createuserAction(Request $request)
    {

        $logger = $this->get('xsmtpdk_logger'); //Custom logger
        $repository = $this->getRepository();
        $xsUsers = new xsUsers($repository, $logger, $this);

        //$post_data = $request->request->all(); //get all POST form data (not the case)
        $post_body = file_get_contents('php://input');

        //XPL_DEBUG
        $logger->debug(__METHOD__ . "\n" . 'New post raw body received: ' . print_r($post_body, true));
        //\\//

        //try to decode the JSON
        $json_data = json_decode($post_body, true);

        //errorcheck JSON sanity
        if (json_last_error() != JSON_ERROR_NONE) {
            return new Response(xsWrapper::assemble('createuser', 'Error', 'invalid JSON data !')->toJson());
        }
        //errorcheck look for userdata
        if (!isset($json_data['userdata'])) {
            return new Response(xsWrapper::assemble('createuser', 'Error', 'missing userdata variable !')->toJson());
        }
        $xs_account_data = $json_data['userdata'];
        //errorcheck if the user login is empty
        if (!isset($xs_account_data['login'])) {
            return new Response(xsWrapper::assemble('createuser', 'Error', "user login missing !")->toJson());
        }
        if (strlen($xs_account_data['login']) < 3) {
            return new Response(xsWrapper::assemble('createuser', 'Error', "user login too small !")->toJson());
        }
        //errorcheck if the user exists already
        if ($xsUsers->userExists($xs_account_data['login'])) {
            return new Response(xsWrapper::assemble('createuser', 'Error', 'user login already exists !')->toJson());
        }
        //XPL_DEBUG
        $logger->debug(__METHOD__ . "\n" . 'New userdata received: ' . print_r($xs_account_data, true));
        //\\//
        $xsUsers->createUser($xs_account_data);

        $response = new JsonResponse();
        $response->setData(xsWrapper::assemble('createuser', $xs_account_data['login'], 'OK')->toArray());
        return $response;
    }

//////////////////////////////////////////////
    public function example_createuserAction()
    {
        $rnd = rand();
        $example_data = array(
            'userdata' => array(
                'email' => 'cucu' . $rnd . '@example.com',
                'login' => 'cucu' . $rnd . '@example.com',
                'password' => 'tester123',
                'first_name' => 'mohamad',
                'last_name' => 'ali',
                'postal_code' => '061234',
                'address' => 'Emirates Holidays Building,
                                    Sheikh Zayed Road
                                    P.O Box 7631',
                'city' => 'Deva',
                'company' => 'Boston Dynamics',
                'country' => 'United Arab Emirates',
                'creation_date' => '2014-07-31 16:25:38',
                'telephone' => '+971 4 214 4888',
                'lang' => 'eng-GB',
            ),
        );
        return new Response(json_encode($example_data));
    }

    //////////////////////////////////////////////
    public function getcurrentuserloginAction()
    {
        $repository = $this->getRepository(); //get the repo
        $logger = $this->get('xsmtpdk_logger'); //Custom logger

        $xsUsers = new xsUsers($repository, $logger, $this);
        $currentUser = $xsUsers->getCurrentUserLogin();
        return new Response(xsWrapper::assemble('getcurrentuserlogin', "", $currentUser)->toJson());
    }

    //////////////////////////////////////////////
    public function getbufferemailsAction()
    {
        $repository = $this->getRepository();

        $xsBufferEmails = new xsBufferEmails($repository);
        $xsBufferEmails_list = $xsBufferEmails->listBufferEmails();

        return new Response(xsWrapper::assemble('getbufferemails', "", $xsBufferEmails_list)->toJson());
    }

    //////////////////////////////////////////////
    public function getsubscriptionsAction($name)
    {
        //Format the string first
        $name = trim(strtolower($name));
        $repository = $this->getRepository();
        $searchService = $repository->getSearchService();
        $locationService = $repository->getLocationService();
        $urlAliasService = $repository->getURLAliasService();
        $contentTypeService = $repository->getContentTypeService();

        $xsSubsc = new xsSubscription($repository, $name);
        $xsSubsc_full = $xsSubsc->listSubscriptionsTaggedFull(array('TagOrderIndex' => true,));
        //$xsSubsc->copySubscription('basic1', 295);


        return new Response(xsWrapper::assemble('getsubscriptions', $name, $xsSubsc_full)->toJson());
        //return new Response("===END===");


        /*
        $subscriptions_id = $oneResult->__get('id');
        $subscriptions_location= $locationService->loadLocation( $subscriptions_id )->pathString;
        //\Doctrine\Common\Util\Debug::dump($subscriptions_location);
        */

    }
    //////////////////////////////////////////////
    //type can be: base, upgrade
    //name: is the suid of the subscription
    public function getasubscriptionAction($type, $suid)
    {
        //Format the string first
        $suid = trim(strtolower($suid));
        $type = trim(strtolower($type));

        $repository = $this->getRepository();

        /* //not needed here
        $searchService = $repository->getSearchService();
        $locationService = $repository->getLocationService();
        $urlAliasService = $repository->getURLAliasService();
        $contentTypeService = $repository->getContentTypeService();
        */

        $xsSubsc = new xsSubscription($repository, $type);
        $xsSubsc_one = $xsSubsc->getSubscription($suid);

        return new Response(xsWrapper::assemble('getasubscription', "$type/$suid", $xsSubsc_one)->toJson());
    }

    //////////////////////////////////////////////
    //base_type can be: base_shared, base_dedicated (it is the base subscription folder)
    //base_suid: is the suid of the base subscription
    //upgrade_type can be: upgrades_shared, upgrades_dedicated (it is the upgrade subscription folder, where we look for upgrade subscriptions with relation to the base subscription indicated
    //example call:   xsmtpdk/api/getsubscriptionupgrades/base_shared/basic1/upgrade_shared

    public function getsubscriptionupgradesAction($base_type, $base_suid, $upgrade_type)
    {
        //Format the string first
        $base_suid = trim(strtolower($base_suid));
        $base_type = trim(strtolower($base_type));
        $upgrade_type = trim(strtolower($upgrade_type));

        $repository = $this->getRepository();

        $xsSubsc = new xsSubscription($repository, $upgrade_type);
        $xsSubsc_upgrades = $xsSubsc->getSubscriptionUpgrades($base_type, $base_suid, $upgrade_type);

        return new Response(xsWrapper::assemble('getsubscriptionupgrades', "$base_type/$base_suid/$upgrade_type", $xsSubsc_upgrades)->toJson());
    }

    //////////////////////////////////////////////
    public function createorderpendingAction(Request $request)
    {
        $logger = $this->get('xsmtpdk_logger'); //Custom logger
        $repository = $this->getRepository(); //get the repo

        $post_body = file_get_contents('php://input');
        $json_data = json_decode($post_body, true);
        $user_data = $json_data['userdata'];
        $subscription_data = $json_data['subscriptiondata'];
        $payment_data = $json_data['paymentdata'];
        $total_price = 0;

        //XPL_DEBUG
        //$logger->info(__METHOD__." JSON request: "."\n". print_r($json_data, true));
        //

        $xsOrder = new xsOrders($repository, $logger);
        $xsUsers = new xsUsers($repository, $logger, $this);
        if ($subscription_data['base_suid']) {
            $xsBaseSubsc = new xsSubscription($repository, $subscription_data['base_type']);
            $xsBaseSubsc_one = $xsBaseSubsc->getSubscription($subscription_data['base_suid']);
            if (isset($xsBaseSubsc_one['extensions']['xs_price_tag'])) $total_price += $xsBaseSubsc_one['extensions']['xs_price_tag'][$payment_data["currency"]];
        }
        if ($subscription_data['upgrade_suid']) {
            $xsUpgradeSubsc = new xsSubscription($repository, $subscription_data['upgrade_type']);
            $xsUpgradeSubsc_one = $xsUpgradeSubsc->getSubscription($subscription_data['upgrade_suid']);
            if (isset($xsUpgradeSubsc_one['extensions']['xs_price_tag'])) $total_price += $xsUpgradeSubsc_one['extensions']['xs_price_tag'][$payment_data["currency"]];
        }
        if ($subscription_data['buffer_emails_relays']) {
            $xsBufferEmails = new xsBufferEmails($repository);
            $xsBufferEmails_list = $xsBufferEmails->listBufferEmails();
            $total_price += $xsBufferEmails_list[$subscription_data['buffer_emails_relays']][$payment_data["currency"]];
        }
        $order_data = array(
            "currency" => $payment_data["currency"],
            "embedded_data" => array('userdata' => $user_data, 'subscriptiondata' => $subscription_data),
            "payment_method" => $payment_data["payment_method"],
            "status" => "pending",
            "total" => $total_price,
            "user_admin_email" => $user_data['initial_admin_email']
        );

        if (!$xsUsers->userExists($user_data['initial_admin_email'])) {
            $cleaned_orders_no = $xsOrder->cleanupPendingOrders($user_data['initial_admin_email']);
            //XPL_DEBUG
            if ($cleaned_orders_no > 0) $logger->info(__METHOD__ . " Deleted " . $cleaned_orders_no . " order(s) pending with admin_email: " . $user_data['initial_admin_email']);
            //
        }
        $newOrderID = $xsOrder->createOrder($order_data);
        //XPL_DEBUG
        $logger->info(__METHOD__ . " New order (" . $newOrderID . ") created pending with admin_email: " . $user_data['initial_admin_email']);
        //
        return new Response(xsWrapper::assemble('createorderpending', $user_data['initial_admin_email'], array('status' => 'OK', 'orderid' => $newOrderID))->toJson());

    }

    //////////////////////////////////////////////
    public function getorderpendingAction($orderid)
    {
        //Format the string first, into an integer
        $orderid = (int)trim(strtolower($orderid));

        $logger = $this->get('xsmtpdk_logger'); //Custom logger
        $repository = $this->getRepository(); //get the repo

        //get the order
        $xsOrder = new xsOrders($repository, $logger);
        $xsOrder_one = $xsOrder->getOrder($orderid);
        if (is_bool($xsOrder_one)) {
            return new Response(xsWrapper::assemble('getorderpending', "Error", "OrderID: " . $orderid . " not found")->toJson());
        }

        //get the compound subscription data (from base and upgrade)
        $subscription_data = $xsOrder_one['embedded_data']['subscriptiondata'];
        $xsSubsc = new xsSubscription($repository, $subscription_data['base_type']);
        $xsOrder_one['compound_subscription'] = $xsSubsc->getCompoundSubscription($subscription_data['base_type'], $subscription_data['base_suid'], $subscription_data['upgrade_type'], $subscription_data['upgrade_suid']);

        //Convert the country from A2 to full text Country string
        $ezCountry = $this->get("ezpublish.fieldtype.ezcountry");
        $userCountryA2 = $xsOrder_one['embedded_data']['userdata']['country'];
        $xsOrder_one['embedded_data']['userdata']['country'] = $ezCountry->fromHash(array($userCountryA2))->__toString();

        //Generate the ePayData values
        $epayCallbackURL = '';
        //devel, create callbackURL using the ez5-redirect server
        switch ($_SERVER['SERVER_PORT']) {
            case 17244 :
                $epayCallbackURL = 'http://ez5-redirect.expertlance.eu/epaycallback.php/' . 'dev2' . '/xsmtpdk/api/epaycallback';
                break;
            case 17344 :
                $epayCallbackURL = 'http://ez5-redirect.expertlance.eu/epaycallback.php/' . 'dev3' . '/xsmtpdk/api/epaycallback';
                break;
            case 17444 :
                $epayCallbackURL = 'http://ez5-redirect.expertlance.eu/epaycallback.php/' . 'dev4' . '/xsmtpdk/api/epaycallback';
                break;

        }
        //generate the acceptURL (http or https)
        $epayAcceptURL = $_SERVER['HTTP_HOST'] . '/payment_accept';
        if ($_SERVER['HTTPS'] == 'on') $epayAcceptURL = 'https://' . $epayAcceptURL;
        else $epayAcceptURL = 'http://' . $epayAcceptURL;

        //epay parameters
        $xsOrder_one["epay_values"] = array(
            'windowstate' => 1,
            'paymentcollection' => 0,
            'paymenttype' => '3,4,6,7,14',
            'subscription' => 1, //tells epay to store the client card data, for recurrent payments
            'instantcapture' => 1, // Enable this parameter to capture payments instantly (as soon as they are authorised)
            'timeout' => 15,
            'callbackurl' => $epayCallbackURL,
            'accepturl' => $epayAcceptURL,
            'merchantnumber' => "8014476",
            'amount' => (int)$xsOrder_one['total'] * 100,
            'currency' => $xsOrder_one['currency'],
            'language' => $xsOrder_one['currency'] == "eur" ? 2 : 1,
            'orderid' => $xsOrder_one['orderid'],
            //description appears in ordertext instead of "description"
            'ordertext' => 'User account: ' . $xsOrder_one['user_admin_email'] . "<br>" . "Subscription: " . $xsOrder_one['compound_subscription']['title'],

        );
        //Generate the Hash key based on ePay algorithm and secret MD5 key (configured in epay admin)
        $epayHash = md5(implode("", array_values($xsOrder_one["epay_values"])) . $this->epayMD5Key);
        $xsOrder_one["epay_values"]['hash'] = $epayHash;
        $xsOrder_one['expiry_date_hr'] = date("Y-m-d H:i:s", (int)($xsOrder_one['date'] + $xsOrder_one['compound_subscription']['extensions']['xs_expiry'] * 86400));

        return new Response(xsWrapper::assemble('getorderpending', "$orderid", $xsOrder_one)->toJson());
    }

    //////////////////////////////////////////////
    public function epaycallbackAction(Request $request)
    {
        $logger = $this->get('xsmtpdk_logger'); //Custom logger
        $repository = $this->getRepository(); //get the repo

        //generate the epayParams array without the 'hash' key
        $queryParams = $request->query->all();
        //XPL_DEBUG
        $logger->info(__METHOD__ . " ePayAcceptCallback Params" . "\n" . print_r($queryParams, true));
        $epayParams = array_diff_key($queryParams, array('hash' => "")); //remove the hash key
        $epayHash = md5(implode("", array_values($epayParams)) . $this->epayMD5Key);

        if (!isset($queryParams['hash'])) {
            $logger->info(__METHOD__ . " Error: hash missing !");
            return new Response(xsWrapper::assemble('epaycallback', "Error", "hash missing !")->toJson());
        } elseif ($epayHash != $queryParams['hash']) {
            $logger->info(__METHOD__ . " Error: hash mismatch !");
            return new Response(xsWrapper::assemble('epaycallback', "Error", "hash mismatch !")->toJson());
        } elseif (!isset($epayParams['orderid'])) {
            $logger->info(__METHOD__ . " Error: OrderID missing !");
            return new Response(xsWrapper::assemble('epaycallback', "Error", "OrderID missing")->toJson());
        }

        $orderid = $epayParams['orderid'];
        //get the order
        $xsOrder = new xsOrders($repository, $logger);
        $xsOrder_one = $xsOrder->getOrder($orderid);
        if (is_bool($xsOrder_one)) {

            return new Response(xsWrapper::assemble('epaycallback', "Error", "OrderID: " . $orderid . " not found")->toJson());
        }
        //get the user and subscription data
        $user_data = $xsOrder_one['embedded_data']['userdata'];
        $subscription_data = $xsOrder_one['embedded_data']['subscriptiondata'];
        $xsUsers = new xsUsers($repository, $logger, $this);
        if ($xsUsers->userExists($user_data['initial_admin_email'])) {
            return new Response(xsWrapper::assemble('epaycallback', 'Error', 'user login already exists !')->toJson());
        }
        //create the user and capture the container node and object IDs
        $xsUserIDs = $xsUsers->createUser($user_data);
        //copy the base_subscription
        $xsSubsc = new xsSubscription($repository, $subscription_data['base_type']);
        $xsSubsc->copySubscription($subscription_data['base_suid'], $xsUserIDs['subscriptions_node_id']);
        /*
        //copy the upgrade subscription
        $xsSubsc = new xsSubscription($repository, $subscription_data['upgrade_type']);
        $xsSubsc->copySubscription($subscription_data['upgrade_suid'], $xsUserIDs['subscriptions_node_id'] );
        */

        return new Response('nimic');
    }

    //////////////////////////////////////////////
    public function testAction()
    {
        //$name=trim(strtolower($name));
        //Logging methods are here: https://github.com/php-fig/log/blob/master/Psr/Log/LoggerInterface.php
        //Available logging methods: (emergency alert critical error warning notice info debug log
        //$logger = $this->get('logger');  //Default logger

        $logger = $this->get('xsmtpdk_logger'); //Custom logger


        //$logger->debug(__METHOD__."\n".print_r($logger, true));

        //$logger->info("Context stuff as json", array("smth" => array(1,2)));

        return new Response(print_r($ezCountry->fromHash(array('ROU')), true));
    }

    //////////////////////////////////////////////
    public function goodnessAction(Request $request)
    {
        $logger = $this->get('xsmtpdk_logger'); //Custom logger

        $post_body = file_get_contents('php://input');

        $json_data = json_decode($post_body, true);

        $repository = $this->getRepository();

        $userService = $repository->getUserService();


        $userID = $userService->loadUserByLogin($json_data['login'])->versionInfo->contentInfo->id; // try catch

        $logger->debug(__METHOD__ . "\n" . 'Login from JSON here  ' . print_r($json_data['login'], true));

        // Content info
        $contentService = $repository->getContentService();
        // get hte UserID somehow
        $contentInfo = $contentService->loadContentInfo($userID);


        $locationService = $repository->getLocationService();
        $urlAliasService = $repository->getURLAliasService();

        $locations = $locationService->loadLocations($contentInfo);

        $logger->debug(__METHOD__ . "\n" . 'Locations start here === ');

        $location_id = "";

        foreach ($locations as $location) {

            // title match for children
            $urlAlias = $urlAliasService->reverseLookup($location);
            $logger->debug(__METHOD__ . "\n" . 'Locations vvvv ' . print_r($location->pathString, true) . "  " . print_r($urlAlias->path, true));

//            $logger->debug(__METHOD__."\n".'Children locations  '.print_r($locationService->loadLocationChildren( $location ), true));
            // locations[0] is Extensions locations[1] is Payments
            //   $logger->debug(__METHOD__."\n".'Get the Subscriptions!!!!  '.print_r($locationService->loadLocationChildren( $location )->locations[2]->id, true));
            // this is what we need, bro
//            $logger->debug(__METHOD__."\n".'======== Location contentID here  '.print_r($locationService->loadLocationChildren( $location )->locations[2]->contentId, true));
            $logger->debug(__METHOD__ . "\n" . '======== Location contentID here  ' . print_r($locationService->loadLocationChildren($location)->locations[2]->id, true));


            //$meh = $locationService->loadLocationChildren( $location )->locations[2]->id;


            $location_id = $locationService->loadLocationChildren($location)->locations[2]->id;
            //$locationService->loadLocationChildren( $location )->attributes();
            //$locationService->loadLocationChildren( $location )->locations(1);
        }


        // get to the subscription
        $type = $json_data['subscriptiondata']['base_type'];
        $suid = $json_data['subscriptiondata']['base_suid'];


        $logger->debug(__METHOD__ . "\n" . 'Type is ' . print_r($type, true) . "  and suid is   " . print_r($suid, true));

        $xsSubsc = new xsSubscription($repository, $type);

        //$xsSubsc_one = $xsSubsc->getSubscription($suid);

        $xsSubsc->copySubscription($suid, $location_id);

        $logger->debug(__METHOD__ . "\n" . 'Copied some shit in ' . $location_id);

        return new Response(xsWrapper::assemble('goodness', "${json_data['login']}/$suid/$type", 'OK')->toJson());

        return new Response(json_encode(array('ok' => $json_data['login'])));

    }

}
