<?php
// XPL_MOD
/**
 * File containing the TopMenu Component class
 *
 * @author Thiago Campos Viana <thiagocamposviana@gmail.com>
 */

namespace Tutei\BaseBundle\Classes\Components;

use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\LocationPriority;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Operator;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion\Visibility;
use Symfony\Component\HttpFoundation\Response;
use Tutei\BaseBundle\Classes\SearchHelper;

//XPL_PATCH includes for visibility and field criterion

/**
 * Renders page TopMenu
 */
class TopMenu extends Component
{

    /**
     * {@inheritDoc}
     */
    public function render()
    {

        $response = new Response();
        $rootLocationId = $this->controller->getConfigResolver()->getParameter('content.tree_root.location_id');

        $response->setPublic();
        $response->setSharedMaxAge(86400);

        // Menu will expire when top location cache expires.
        $response->headers->set('X-Location-Id', $rootLocationId);
        // Menu might vary depending on user permissions, so make the cache vary on the user hash.
        $response->setVary('X-User-Hash');

        $classes = $this->controller->getContainer()->getParameter('project.menufilter.top');

        $filters = array(
            SearchHelper::createMenuFilter($classes),
            new LocationPriority(Operator::LT, 100),
            //XPL_PATCH added filters (hide Dashboard and Checkout, and only show Visible items 
            new Criterion\LogicalNot(new Criterion\Field("name", Criterion\Operator::EQ, "Checkout")),
            new Criterion\LogicalNot(new Criterion\Field("name", Criterion\Operator::EQ, "Dashboard")),
            new Criterion\LogicalNot(new Criterion\Field("name", Criterion\Operator::EQ, "Dashboard_legacy")),
            new Criterion\LogicalNot(new Criterion\Field("name", Criterion\Operator::EQ, "payment_accept")),
            new Criterion\LogicalNot(new Criterion\Field("name", Criterion\Operator::EQ, "gratis_account")),
            new Visibility(Visibility::VISIBLE),
            //\\//
        );

        $list = SearchHelper::fetchChildren($this->controller, $rootLocationId, $filters);

        $pathString = '';
        if (isset($this->parameters['pathString'])) {
            $pathString = $this->parameters['pathString'];
        }

        $locations = explode('/', $pathString);

        $locationChildren = array();

        if ($this->controller->getContainer()->getParameter('project.topmenu.dropdown')) {
            foreach ($list->searchHits as $item) {
                $locationdId = $item->valueObject->versionInfo->contentInfo->mainLocationId;
                $searchResult = SearchHelper::fetchChildren($this->controller, $locationdId, $filters);

                if ($searchResult->totalCount > 0) {
                    $locationChildren[$locationdId] = $searchResult;
                }
            }
        }


        return $this->controller->render(
            'TuteiBaseBundle:parts:top_menu.html.twig', array(
            'list' => $list,
            'locations' => $locations,
            'location_children' => $locationChildren,
        ), $response
        );
    }

}
