#!/bin/bash

function gen_random_username() {
	local wordfile=/root/names.txt
	local randwords
	local title
 
	if [[ $(which shuf) ]]; then
		randwords=`shuf -n 2 $wordfile|sed 's/[^a-zA-Z]//g'`
		title=`echo $randwords|tr ' ' '_'`
	else
		cat <<-EOS
		$FUNCNAME requires the utility shuf (or gshuf) to generate random names.
		Use homebrew to install "coreutils," which includes shuf.
		EOS
		return
	fi
	echo ${title}
}

function name() {
	local wordfile=/root/names.txt
	local randwords
	local title

	if [[ $(which shuf) ]]; then
		randwords=`shuf -n 1 $wordfile|sed 's/[^a-zA-Z]//g'`
	fi

	echo $randwords
}




function randomString32() {

	index=0

	str=""

	for i in {a..z}; do arr[index]=$i; index=`expr ${index} + 1`; done

	for i in {A..Z}; do arr[index]=$i; index=`expr ${index} + 1`; done

	for i in {0..9}; do arr[index]=$i; index=`expr ${index} + 1`; done

	for i in {1..64}; do str="$str${arr[$RANDOM%$index]}"; done

	echo $str

}


function vowel() {
    s=aeoiu
    p=$(( $RANDOM % 5))
    echo -n ${s:$p:1}
}

function consonant() {
	s=bcdfghjklmnpqrstvxz 
	p=$(( $RANDOM % 5))
	echo -n ${s:$p:1}
}


export -f vowel
export -f consonant
export -f randomString32
export -f name
export -f gen_random_username
