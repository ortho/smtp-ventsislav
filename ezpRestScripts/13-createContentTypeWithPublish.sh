source /root/rest.cfg


random=$[ ( $RANDOM % 1000 )  + 1 ]
identifier="newContentTypeWithPublish_"$random

function randomString32() {

	index=0

	str=""

	for i in {a..z}; do arr[index]=$i; index=`expr ${index} + 1`; done

	for i in {A..Z}; do arr[index]=$i; index=`expr ${index} + 1`; done

	for i in {0..9}; do arr[index]=$i; index=`expr ${index} + 1`; done

	for i in {1..64}; do str="$str${arr[$RANDOM%$index]}"; done

	echo $str

}


randomID=`randomString32`

echo '{
  "ContentTypeCreate": {
    "identifier": "'$identifier'",
    "mainLanguageCode": "eng-GB",
    "remoteId": "remoteId'$randomID'",
    "urlAliasSchema": "<title>",
    "nameSchema": "<title>",
    "isContainer": "true",
    "defaultSortField": "PATH",
    "defaultSortOrder": "ASC",
    "defaultAlwaysAvailable": "true",
    "names": {
      "value": [
        {
          "_languageCode": "eng-GB",
          "#text": "New Content Type '$random'"
        }
      ]
    },
    "descriptions": {
      "value": [
        {
          "_languageCode": "eng-GB",
          "#text": "This is a description"
        }
      ]
    },
    "modificationDate": "2014-12-31T12:30:00",
    "User": {
      "_href": "/api/ezp/v2/user/users/14"
    },
    "FieldDefinitions": {
      "FieldDefinition": [
        {
          "identifier": "title",
          "fieldType": "ezstring",
          "names": {
            "value": [
              {
                "_languageCode": "eng-GB",
                "#text": "Title"
              }
            ]
          },
          "descriptions": {
            "value": [
              {
                "_languageCode": "eng-GB",
                "#text": "This is the title"
              }
            ]
          },
          "fieldGroup": "content",
          "position": "1",
          "isTranslatable": "true",
          "isRequired": "true",
          "isInfoCollector": "false",
          "isSearchable": "true",
          "defaultValue": "New Title"
        },
        {
          "identifier": "summary",
          "fieldType": "eztext",
          "names": {
            "value": [
              {
                "_languageCode": "eng-GB",
                "#text": "Summary"
              }
            ]
          },
          "descriptions": {
            "value": [
              {
                "_languageCode": "eng-GB",
                "#text": "This is the summary"
              }
            ]
          },
          "fieldGroup": "content",
          "position": "2",
          "isTranslatable": "true",
          "isRequired": "false",
          "isInfoCollector": "false",
          "isSearchable": "true"
        }
      ]
    }
  }
}' | \
http --auth $username:$password -v POST $host/api/ezp/v2/content/typegroups/1/types?publish=true \
'Accept:application/vnd.ez.api.ContentType+json' \
'Content-Type:application/vnd.ez.api.ContentTypeCreate+json' --verify=no  # don't verify the https:// cert 

